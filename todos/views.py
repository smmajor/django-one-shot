from django.shortcuts import render, redirect, get_object_or_404
from todos.forms import TodoListForm, TodoItemForm
from .models import TodoList, TodoItem

# Create your views here.


def todo_list(request):
    todolist = TodoList.objects.all()
    context = {
        "todo_list_object": todolist,
    }

    return render(request, "todos/list.html", context)


def list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "list_object": list,
    }

    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
         "form": form,
        }
    return render(request, "todos/create.html", context)


def update_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todolist)
    context = {
        "list_object": todolist,
        "todo_list_form": form
    }

    return render(request, "todos/update.html", context)


def delete_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/items/create.html", context)


def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)
    context = {
        "form": form
    }
    return render(request, "todos/items/update.html", context)
